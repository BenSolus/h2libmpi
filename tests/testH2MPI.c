#define DIM 1024

#include <mpi.h>

#include "clusterMPI.h"
#include "gravitation.h"
#include "h2matrixMPI.h"

#include "block.h"

int main()
{
  MPI_Init(NULL, NULL);

  pamatrix         G;
  pblock           b;
  pclusterbasis    cb;
  pclustergeometry cg;
  pdstbcluster     dc;
  pgrav            gv;
  ph2matrix        H2;

  int              world_rank;
  real             eta;
  uint             csp;
  uint*            idx;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  gv  = new_grav(DIM, 5);

  random_distributed_grav(gv);

  cg  = build_from_grav_clustergeometry(gv);

  idx = (uint *) allocmem((size_t) gv->n * sizeof(uint));

  for(uint i = 0; i < gv->n; ++i)
    idx[i] = i;

  dc  = build_distributed_adaptive_cluster(cg, cg->nidx, idx, 16);

  printf("  Node %d (cluster tree): %u clusters\n", world_rank, dc->c.desc);

  eta = 1.0;

  b   = build_strict_block((pcluster) dc,
                           (pcluster) dc,
                           &eta,
                           admissible_2_cluster);

  csp = compute_csp_block(b);

  printf("  Node %d (block cluster tree): %u blocks\n"
         "                                Sparsity %u\n", world_rank, b->desc, csp);

  G = new_amatrix(gv->n, gv->n);

  nearfield_grav(0, 0, gv, G);

  printf("  Node %d (full matrix): %.1f KB\n", world_rank,
                                               getsize_amatrix(G) / 1024.0);


  cb = build_from_grav_clusterbasis(dc, gv);

  printf("  Node %d (cluster basis): Rank %u\n"
         "                           %.1f KB\n",
         world_rank,
         cb->k,
         getsize_clusterbasis(cb) / 1024.0);

  H2 = build_from_distributed_block_h2matrix(b, cb, cb);

  printf("  Node %d (H2-Matrix): %u blocks\n"
         "                       %.1f KB\n",
         world_rank,
         H2->desc,
         getsize_h2matrix(H2) / 1024.0);

  fill_from_grav_h2matrix(H2, gv);


  real abs = norm2diff_amatrix_distributed_h2matrix(H2, G);
  real rel = abs / norm2_amatrix(G);

  assert(abs < 0.01);
  assert(rel < 0.00001);

  printf("Error: %e (relative: %e)\n", abs, rel);

  del_h2matrix(H2);
  del_amatrix(G);
  del_block(b);
  del_dstbcluster(dc);
  freemem(idx);
  del_clustergeometry(cg);
  del_grav(gv);

  MPI_Finalize();

  return 0;
}
