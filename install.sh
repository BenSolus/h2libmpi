#!/usr/bin/bash

# Path to first CMakeLists.txt
SRCPATH=$(dirname $(realpath $0))

H2INC=""
H2LINK=""

USAGE="  Usage: $0 -i include_path -l link_path\n\n  Where:\n\t-i include_path  Path to the include directory of the H2Lib\n\t-l link_path     Path to the link directory of the H2Lib"

# Command line arguments to get the include and link path of the H2Lib
while getopts i:l: opt
do
  case $opt in
    i) H2INC=$OPTARG;;
    l) H2LINK=$OPTARG;;
    *) echo -e "$USAGE" >&2
       exit 1;;
  esac
done

if [ -z "$H2INC" ]
then
  echo "install.sh: No include path given" >&2
  echo -e $USAGE >&2
  exit 1
elif [ -z "$H2LINK" ]
then
  echo "install.sh: No link path given" >&2
  echo -e $USAGE >&2
  exit 1
fi

mkdir -p "/tmp/build_h2libmpi"

cd "/tmp/build_h2libmpi" || exit

cmake -DCMAKE_BUILD_TYPE=Release -DH2INC:PATH=$H2INC -DH2LINK:PATH=$H2LINK $SRCPATH

make

cp "/tmp/build_h2libmpi/lib/libh2mpi.a" "$H2LINK"

mkdir -p $H2INC/mpi
find $SRCPATH/src/ -name '*.h' -exec cp -pr {} $H2INC/mpi \;

mkdir -p $H2LINK/Examples/mpi
cp -p $SRCPATH/exmp/gravitation.c $H2LINK/Examples/mpi
cp -p $SRCPATH/exmp/gravitation.h $H2LINK/Examples/mpi
