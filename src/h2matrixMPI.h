/* ------------------------------------------------------------
 * This is the file "h2matrixMPI.h" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

/**
 * @file h2matrixMPI.h
 * @author Bennet Carstensen
 */

#ifndef H2MATRIX_MPI_H
#define H2MATRIX_MPI_H

/** @defgroup h2matrixMPI h2matrixMPI
 *  @brief Extension of \f$\mathcal{H}^2\f$-matrices using MPI.
 *
 * Extends the \f$\mathcal{H}^2\f$-matrix class with functions to operate
 * on distributed systems.
 *  @{ */

#include "block.h"
#include "clusterbasis.h"
#include "h2matrix.h"

/** @brief Build an h2matrix object from a block tree using
 *  given cluster bases using MPI.
 * *
 *  @param b Block tree.
 *  @param rb Row cluster basis.
 *  @param cb Column cluster basis.
 *  @returns New h2matrix object. */
HEADER_PREFIX ph2matrix
build_from_distributed_block_h2matrix(pcblock       b,
                                      pclusterbasis rb,
                                      pclusterbasis cb);

/** @brief Interaction phase of the distributed matrix-vector multiplication
 *
 * If the <tt>rb</tt> is responsible then compute either the nearfield blocks
 * or the farfield blocks.
 * If <tt>rb</tt> is active and has sons then call the function recursively.
 *
 *  @param alpha Scaling factor \f$\alpha\f$.
 *  @param h2    Distributed H2-Matrix @f$A@f$.
 *  @param xt    Coefficients \f$(\hat x_s)_{s\in\mathcal{T}_{\mathcal J}}\f$
 *               of the source vector with respect to the
 *               column basis <tt>h2->cb</tt>.
 *  @param yt    Coefficients \f$(\hat y_t)_{t\in\mathcal{T}_{\mathcal I}}\f$
 *               of the target vector with respect to the
 *               row basis <tt>h2->rb</tt>.
 */
HEADER_PREFIX void
fastaddeval_distributed_h2matrix_avector(field      alpha,
                                         pch2matrix h2,
                                         pavector   xt,
			                                   pavector   yt);

/** @brief Interaction phase of the adjoint matrix-vector multiplication
 *
 *  If the <tt>rb</tt> is responsible the compute either the nearfield blocks
 * or the farfield blocks.
 *  If <tt>rb</tt> is active and has sons then call the function recursively.
 *
 *
 *  @param alpha Scaling factor \f$\alpha\f$.
 *  @param h2    Distributed H2-Matrix \f$A\f$.
 *  @param xt    Coefficients \f$(\hat x_t)_{t\in\mathcal{T}_{\mathcal I}}\f$
 *               of the source vector with respect to the
 *               row basis <tt>h2->rb</tt>.
 *  @param yt    Coefficients \f$(\hat y_s)_{s\in\mathcal{T}_{\mathcal J}}\f$
 *               of the target vector with respect to the
 *               column basis <tt>h2->cb</tt>.
 */
HEADER_PREFIX void
fastaddevaltrans_distributed_h2matrix_avector(field      alpha,
                                              pch2matrix h2,
                                              pavector   xt,
                                              pavector   yt);

/** @brief Matrix-vector multiplication
 *  \f$y \gets y + \alpha A x\f$ with an distributed A.
 *
 * The distributed function has in contrast to the normal
 * addeval_h2matrix_avector-function a broadcast step before the
 * fastaddeval-function such that every node has the same informations.
 *
 *  @param alpha Scaling factor \f$\alpha\f$.
 *  @param h2    Distributed H2-Matrix \f$A\f$.
 *  @param x     Source vector \f$x\f$.
 *  @param y     Target vector \f$y\f$. */
HEADER_PREFIX void
addeval_distributed_h2matrix_avector(field         alpha,
                                     pch2matrix    h2,
                                     pcavector     x,
                                     pavector      y);

/** @brief Adjoint matrix-vector multiplication
 *  \f$y \gets y + \alpha A^* x\f$ with an distributed A.
 *
 * The distributed function has in contrast to the normal
 * addevaltrans_h2matrix_avector-function an allreduce step after the
 * fastaddevaltrans-function to add all resultvectors to one vector. This
 * vector will be distributed to all nodes.
 *
 *  @param alpha Scaling factor \f$\alpha\f$.
 *  @param h2    Distributed H2-Matrix \f$A\f$.
 *  @param x     Source vector \f$x\f$.
 *  @param y     Target vector \f$y\f$. */
HEADER_PREFIX void
addevaltrans_distributed_h2matrix_avector(field         alpha,
                                          pch2matrix    h2,
                                          pcavector     x,
                             	            pavector      y);

/** @brief Matrix-vector multiplication
 *  \f$y \gets y + \alpha A x\f$ or \f$y \gets y + \alpha A^* x\f$ using MPI.
 *
 *  @param alpha Scaling factor \f$\alpha\f$.
 *  @param h2trans Set if \f$A^*\f$ is to be used instead of \f$A\f$.
 *  @param h2 Matrix \f$A\f$.
 *  @param x Source vector \f$x\f$.
 *  @param y Target vector \f$y\f$. */
void
mvm_h2matrix_avector_mpi(field      alpha,
                         bool       h2trans,
                         pch2matrix h2,
                         pcavector  x,
                         pavector   y);

/** @brief Approximate the spectral norm \f$\|A-B\|_2\f$ of the difference
 *  of a distributed H2-matrix \f$A\f$ and matrix \f$B\f$ .
 *
 *  @param a Matrix \f$A\f$.
 *  @param b distributed H2-matrix \f$B\f$.
 *  @returns Approximation of \f$\|A-B\|_2\f$. */
HEADER_PREFIX real
norm2diff_amatrix_distributed_h2matrix(pch2matrix a, pcamatrix b);

#endif /* H2MATRIX_MPI_H */
