/* ------------------------------------------------------------
 * This is the file "clusterMPI.c" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

/**
 * @file clusterMPI.c
 * @author Bennet Carstensen
 * @addtogroup clusterMPI
 * @{
 */

#include "clusterMPI.h"

#include "basicMPI.h"

/**@brief  Builds the first two level of the cluster tree.
 *
 * The nodes of the first two level are set manually with the depending
 * responsibility nodes and the activities.
 *
 * @param pdc    Parent cluster or NULL for the root-cluster.
 * @param cf     Clustergeometry object with geometrical information.
 * @param size   Number of indices.
 * @param idx    Index set.
 * @param clf    Maximal leaf size.
 * @param level  Depth of the cluster.
 * @param son    Is the actual node a left (0) or a right (!0) son.
 * @returns Returns an adaptive @ref dstbcluster object.
 */
static pdstbcluster
init_build_distributed_adaptive_cluster(pdstbcluster pdc, pclustergeometry cf, uint size, uint * idx, uint clf, int level, uint son);

/**@brief  Builds distributed cluster trees.
 *
 * Distributes cluster trees to to free nodes until no more nodes are available
 *
 * @param tp     Parent cluster tree.
 * @param cf     Clustergeometry object with geometrical information.
 * @param size   Number of indices.
 * @param idx    Index set.
 * @param clf    Maximal leaf size.
 * @param level  Depth of the cluster.
 * @param parent Rank of the parent node.
 * @param son    Is the actual node a left (0) or a right (!0) son.
 * @param index  Index of the right sons.

 * @returns Returns an adaptive @ref dstbcluster object.
 */
static pdstbcluster
main_build_distributed_adaptive_cluster(pdstbcluster tp, pclustergeometry cf, uint size, uint * idx, uint clf, int level, int parent, uint son, uint index);

/**@brief Build localy available @ref dstbcluster tress
 *
 * If no more nodes are available, the responsibility and activity are set
 * according to the parent cluster
 *
 * @param tp     Parent cluster tree.
 * @param cf     Clustergeometry object with geometrical information.
 * @param size   Number of indices.
 * @param idx    Index set.
 * @param clf    Maximal leaf size.
 * @param level  Depth of the cluster.
 * @param parent Rank of the parent node.
 * @returns Returns an adaptive @ref dstbcluster object.
 */
static pdstbcluster
local_build_distributed_adaptive_cluster(pdstbcluster tp, pclustergeometry cf, uint size, uint * idx, uint clf, int level, int parent);

pdstbcluster
new_dstbcluster(uint         size,
                uint*        idx,
                uint         sons,
                uint         dim,
                pdstbcluster pdc,
                bool         active,
                uint         resp)
{
  cluster   c;
  pdstbcluster dc;

  dc = (pdstbcluster) allocmem((size_t) sizeof(dstbcluster));

  c.size = size;
  c.dim  = dim;
  c.idx  = idx;
  c.bmin = allocreal(dim);
  c.bmax = allocreal(dim);
  c.sons = sons;

  if(sons > 0)
  {
    c.son = (pcluster *) allocmem((size_t) sons * sizeof(pdstbcluster));
    for(uint i = 0; i < sons; ++i)
      c.son[i] = NULL;
  }
  else
    c.son = NULL;

  dc->c      = c;

  dc->pdc    = pdc;
  dc->active = active;
  dc->resp   = resp;

  return dc;
}

void
del_dstbcluster(pdstbcluster dc)
{
  if(dc->c.sons > 0)
    for(uint i = 0; i < dc->c.sons; ++i)
    {
      del_dstbcluster((pdstbcluster) dc->c.son[i]);
      dc->c.son[i] = NULL;
    }
  if(dc->c.son != NULL)
  {
    freemem(dc->c.son);
    dc->c.son = NULL;
  }
  if(dc->c.bmin != NULL)
  {
    freemem(dc->c.bmin);
    dc->c.bmin = NULL;
  }
  if(dc->c.bmax != NULL)
  {
    freemem(dc->c.bmax);
    dc->c.bmax = NULL;
  }
  if(dc->pdc != NULL)
    dc->pdc = NULL;
  if(dc != NULL)
  {
    freemem(dc);
    dc = NULL;
  }
}

pdstbcluster
build_distributed_adaptive_cluster(pclustergeometry cf,
                                   uint             size,
                                   uint*            idx,
                                   uint             clf)
{
  return init_build_distributed_adaptive_cluster(NULL, cf, size, idx, clf, 0, 0);
}

static void
set_parent_active(pdstbcluster dc) {

  if(dc != NULL) {
    dc->active = true;
    set_parent_active(dc->pdc);
  }
}

static pdstbcluster
init_build_distributed_adaptive_cluster(pdstbcluster pdc, pclustergeometry cf, uint size, uint * idx, uint clf, int level, uint son)
{
  pdstbcluster  dc;

  uint      direction;
  uint      size0, size1;
  uint      i, j;
  uint      resp;
  real      a, m;
  int       world_size, world_rank;
  bool      active;

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  active = false;
  resp   = 0;
  
  /* Determine responsibility and activtiy */
  if(level == 0)
  {
    active = true;
    resp   = 0;
  }
  else if(level == 1)
  {
    if(son == 0)
    {
      resp = 0;
      if(world_rank % 2 == 0)
        active = true;
      else
        active = false;
    }
    else
    {
      if(world_size == 1)
      {
        resp = 0;
        active = true;
      }
      else
      {
        resp = 1;
        if(world_rank % 2 == 1)
          active = true;
        else
          active = false;
      }
    }
  }

  if (size > clf)
  {

    update_point_bbox_clustergeometry(cf, size, idx);

    /* compute the direction of partition */
    direction = 0;
    a = cf->hmax[0] - cf->hmin[0];

    for(j = 1; j < cf->dim; j++)
    {
      m = cf->hmax[j] - cf->hmin[j];
      if(a < m)
      {
        a = m;
        direction = j;
      }
    }

    /* build sons */
    if(a > 0.0)
    {
      m = (cf->hmax[direction] + cf->hmin[direction]) / 2.0;
      size0 = 0;
      size1 = 0;

      for(i = 0; i < size; i++)
      {
        if(cf->x[idx[i]][direction] < m)
        {
          j = idx[i];
          idx[i] = idx[size0];
          idx[size0] = j;
          size0++;
        }
        else
          size1++;
      }

      dc = new_dstbcluster(size, idx, 2, cf->dim, pdc, active, resp);

      if(level == 0)
      {
        dc->c.son[0] = (pcluster) init_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, 0);
        dc->c.son[1] = (pcluster) init_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, 1);
      }
      else if(level == 1)
      {
        if(son == 0)
        {
          dc->c.son[0] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, 0, 0, 0);
          dc->c.son[1] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, 0, 1, 1);
        }
        else {
          if(world_size == 1) {
            dc->c.son[0] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, 0, 0, 0);
            dc->c.son[1] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, 0, 1, 1);
          } else {
            dc->c.son[0] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, 1, 0, 0);
            dc->c.son[1] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, 1, 1, 1);
          }
        }
      }

    update_bbox_cluster((pcluster) dc);
    }
    else {
      assert(a == 0.0);
      dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, resp);
      update_support_bbox_cluster(cf, (pcluster) dc);
    }
  }
  else {
    dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, resp);
    update_support_bbox_cluster(cf, (pcluster) dc);
  }

  update_cluster((pcluster) dc);

  return dc;
}

static pdstbcluster
main_build_distributed_adaptive_cluster(pdstbcluster     pdc,
                                        pclustergeometry cf,
                                        uint             size,
                                        uint*            idx,
                                        uint             clf,
                                        int              level,
                                        int              parent,
                                        uint             son,
                                        uint             index)
{

  pdstbcluster  dc;

  uint      direction;
  uint      size0, size1;
  uint      i, j;
  uint      resp;
  real      a, m;
  int       world_size, world_rank;
  int      right_node;
  bool      active;
  bool      init_sequential;

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  --world_size;

  /* If this is a "right" son, calculate the rank of the responsible node */

  right_node = (1 << (level - 1)) + 2 * index;
  if((parent % 2) == 0)
    right_node -= 2;
  else
    right_node -= 1;

  if(world_size < right_node) {
    init_sequential = true;
    resp = parent;
    if(world_rank == parent)
      active = true;
    else
      active = false;
  }
  else {
    init_sequential = false;
    if(son == 0) {
      resp = parent;
      if(world_rank == parent)
        active = true;
      else
        active = false;
    }
    else {
      resp = right_node;
      if(world_rank == right_node) {
        active = true;
        set_parent_active(pdc);
      }
      else
        active = false;
    }
  }

  if (size > clf) {

    update_point_bbox_clustergeometry(cf, size, idx);

    /* compute the direction of partition */
    direction = 0;
    a = cf->hmax[0] - cf->hmin[0];

    for (j = 1; j < cf->dim; j++) {
      m = cf->hmax[j] - cf->hmin[j];
      if (a < m) {
        a = m;
        direction = j;
      }
    }

    /* build sons */
    if (a > 0.0) {
      m = (cf->hmax[direction] + cf->hmin[direction]) / 2.0;
      size0 = 0;
      size1 = 0;

      for (i = 0; i < size; i++) {
        if (cf->x[idx[i]][direction] < m) {
          j = idx[i];
          idx[i] = idx[size0];
          idx[size0] = j;
          size0++;
        }
        else {
          size1++;
        }
      }

      dc = new_dstbcluster(size, idx, 2, cf->dim, pdc, active, resp);

      if(init_sequential) {
        dc->c.son[0] = (pcluster) local_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1,  parent);
        dc->c.son[1] = (pcluster) local_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, parent);
      }
      else {
        if(son == 0) {
          dc->c.son[0] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, parent, 0, 0);
          dc->c.son[1] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, parent, 1, index+1);
        }
        else {
          dc->c.son[0] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, right_node, 0, 0);
          dc->c.son[1] = (pcluster) main_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, right_node, 1, index+1);
        }
      }

      update_bbox_cluster((pcluster) dc);
    }
    else {
      assert(a == 0.0);
      dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, resp);
      update_support_bbox_cluster(cf, (pcluster) dc);
    }
  }
  else {
    dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, resp);
    update_support_bbox_cluster(cf, (pcluster) dc);
  }

  update_cluster((pcluster) dc);

  return dc;
}

pdstbcluster
local_build_distributed_adaptive_cluster(pdstbcluster pdc, pclustergeometry cf, uint size, uint * idx, uint clf, int level, int parent)
{
  pdstbcluster  dc;

  uint          direction;
  uint          size0, size1;
  uint          i, j;
  real          a, m;
  int           world_rank;
  bool          active;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  if(world_rank == parent)
    active = true;
  else
    active = false;

  if (size > clf) {

    update_point_bbox_clustergeometry(cf, size, idx);

    /* compute the direction of partition */
    direction = 0;
    a = cf->hmax[0] - cf->hmin[0];

    for (j = 1; j < cf->dim; j++) {
      m = cf->hmax[j] - cf->hmin[j];
      if (a < m) {
        a = m;
        direction = j;
      }
    }

    /* build sons */
    if (a > 0.0) {
      m = (cf->hmax[direction] + cf->hmin[direction]) / 2.0;
      size0 = 0;
      size1 = 0;

      for (i = 0; i < size; i++) {
        if (cf->x[idx[i]][direction] < m) {
          j = idx[i];
          idx[i] = idx[size0];
          idx[size0] = j;
          size0++;
        }
        else {
          size1++;
        }
      }

      dc = new_dstbcluster(size, idx, 2, cf->dim, pdc, active, parent);

      dc->c.son[0] = (pcluster) local_build_distributed_adaptive_cluster(dc, cf, size0, idx, clf, level+1, parent);
      dc->c.son[1] = (pcluster) local_build_distributed_adaptive_cluster(dc, cf, size1, idx + size0, clf, level+1, parent);

      update_bbox_cluster((pcluster) dc);
    }
    else {
      assert(a == 0.0);
      dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, parent);
      update_support_bbox_cluster(cf, (pcluster) dc);
    }
  }
  else {
    dc = new_dstbcluster(size, idx, 0, cf->dim, pdc, active, parent);
    update_support_bbox_cluster(cf, (pcluster) dc);
  }

  update_cluster((pcluster) dc);

  return dc;
}

/** @} */
