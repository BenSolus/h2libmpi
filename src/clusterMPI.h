/* ------------------------------------------------------------
 * This is the file "clusterMPI.h" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

/**
 * @file clusterMPI.h
 * @author Bennet Carstensen
 */

#ifndef CLUSTER_MPI_H
#define CLUSTER_MPI_H

/** @defgroup clusterMPI clusterMPI
 *  @brief Representation of a distributed cluster for sending and receiving with MPI.
 *
 * The @ref dstbcluster class adds members to the cluster class which are
 * needed for sending and receiving via MPI..
 * @{
 */

/** @brief Representation of a distributed cluster tree. */
typedef struct _dstbcluster dstbcluster;

/** @brief Pointer to distributed @ref dstbcluster object. */
typedef dstbcluster* pdstbcluster;

/** @brief Pointer to constant distributed @ref dstbcluster object.*/
typedef const dstbcluster *pcdstbcluster;

#include "cluster.h"

/**
 * @brief Representation of distributed cluster trees.
 *
 * Cluster trees are recursively represented labeled trees.
 * The labels are subsets of the index set <tt>idx</tt> and the labels of the
 * sons form a disjunct partition of the label of the father.
 * In addition to the original cluster class the @ref dstbcluster class
 * adds the members @ref resp, which represents the index of the node who is
 * responsible for calculating the Matrix-Vector product of Leaf-, Transfer-
 * and Blockmatrices associated to this cluster.
 * The member @ref active indicates that this node will be responsible for
 * some descendant of this cluster.
 */
struct _dstbcluster
{
  /** @brief The original cluster object */
  cluster c;

  /** @brief Parent cluster or NULL in case of root */
  pdstbcluster pdc;

  /** @brief The index of the node who performs calculating associated with this cluster */
  int resp;

  /** @brief Set if this node is responsible for some descendant of this cluster. */
  bool active;
};

/** @brief Create a new @ref dstbcluster object.
 *
 * Allocates storage for the object and sets the pointers to the sons to NULL.
 *
 * @remark Should always be matched by a call to @ref del_dstbcluster.
 *
 * @param size   Number of indices.
 * @param idx    Index set.
 * @param sons   Number of sons.
 * @param dim    Spatial dimension of bounding box.
 * @param pdc    Parent cluster or NULL in case of root
 * @param resp   The index of the node who performs calculating associated with this cluster
 * @param active Set if this node is responsible for some descendant of this cluster.
 * @returns New @ref dstbcluster object.
 */
HEADER_PREFIX pdstbcluster
new_dstbcluster(uint         size,
                uint*        idx,
                uint         sons,
                uint         dim,
                pdstbcluster pdc,
                bool         active,
                uint         resp);

/** @brief Delete a @ref dstbcluster object.
 *
 * Releases the storage corresponding to the @ref dstbcluster object.
 * If the cluster has sons, their storage is released too.
 *
 * @param dc Distributed cluster object to be deleted.
 */
HEADER_PREFIX void
del_dstbcluster(pdstbcluster dc);

/** @brief Build a cluster tree from a clustergeometry object using adaptive clustering and MPI.
 *
 *
 * Consider the cluster as a binary tree.
 * Consider the sons of the root as roots of two clusters (left/right).
 * In the left cluster, only nodes with an even rank are active or
 * responsible. The same holds for the right cluster and nodes with
 * an odd rank.
 * The responsible node for the "left" son is always the node
 * responsible for the parent.
 * Each "right" son receives an index for the current level. Counting
 * from 0 for the leftmost "right" son of both clusters to the rightmost
 * one. We calculate the responsible node for each "right" son with
 *    \f[2^{level - 1} + 2 * index - 2\f]
 * for nodes in the left cluster and
 *   \f[ 2^{level - 1} + 2 * index - 1\f]
 * for nodes in the right cluster. Example for the distribution of
 * responsibility:
 *
 *                _0_                    level 0
 *              _/   \_
 *            _/       \_
 *         _/             \_
 *        0                 1            level 1
 *      _/ \_             _/ \_
 *    _/     \_         _/     \_
 *    0        2       1         3       level 2
 *  _/ \_    _/ \_   _/ \ _    _/ \_
 * 0     4  2     6 1      5  3     7    level 3
 *
 *       0        1        0        1    indices for the "right" sons on level 3
 *
 * @param cf Clustergeometry object with geometrical information.
 * @param size Number of indices.
 * @param idx Index set.
 * @param clf Maximal leaf size.
 */
HEADER_PREFIX pdstbcluster
build_distributed_adaptive_cluster(pclustergeometry cf,
                                   uint             size,
                                   uint*            idx,
                                   uint             clf);

/** @} */

#endif // CLUSTER_MPI_H
