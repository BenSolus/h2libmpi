/* ------------------------------------------------------------
 * This is the file "h2matrixMPI.c" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

#include "h2matrixMPI.h"

#include "basicMPI.h"
#include "clusterMPI.h"
#include "clusterbasisMPI.h"

ph2matrix
build_from_distributed_block_h2matrix(pcblock       b,
                                      pclusterbasis rb,
                                      pclusterbasis cb)
{
  ph2matrix h;

  h = NULL;

  if (b->son) {
    uint rsons = b->rsons;
    uint csons = b->csons;

    h = new_super_h2matrix(rb, cb, rsons, csons);

    for(uint j = 0; j < csons; j++)
      for(uint i = 0; i < rsons; i++)
      {
        pcblock b1        = b->son[i + j * rsons];
        pclusterbasis rb1 = rb;

        if(b1->rc != b->rc)
        {
          assert(rb->sons == rsons);
          rb1 = rb->son[i];
        }

        pclusterbasis cb1 = cb;

        if (b1->cc != b->cc) {
          assert(cb->sons == csons);
          cb1 = cb->son[j];
        }

        ph2matrix h1 = build_from_distributed_block_h2matrix(b1, rb1, cb1);

        ref_h2matrix(h->son + i + j * rsons, h1);
      }
  }
  else
  {
    pcdstbcluster dc = (pcdstbcluster) rb->t;

    int           world_rank;

    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if(world_rank == dc->resp)
      if(b->a > 0)
        h = new_uniform_h2matrix(rb, cb);
      else
        h = new_full_h2matrix(rb, cb);
    else
      h = new_zero_h2matrix(rb, cb);
  }

  update_h2matrix(h);

  return h;
}

void
fastaddeval_distributed_h2matrix_avector(field      alpha,
                                         pch2matrix h2,
                                         pavector   xt,
			                                   pavector   yt)
{
  const uint     rsons = h2->rsons;
  const uint     csons = h2->csons;
  pcclusterbasis rb    = h2->rb;
  pcclusterbasis cb    = h2->cb;

  avector        loc1, loc2;
  int            world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  pcdstbcluster dc = (pcdstbcluster) rb->t;

  if(world_rank == dc->resp)
  {
    if (h2->u)
    {
      addeval_amatrix_avector(alpha, &h2->u->S, xt, yt);
    }
    else if (h2->f)
    {
      pavector xp = init_sub_avector(&loc1, xt, cb->t->size, cb->k);
      pavector yp = init_sub_avector(&loc2, yt, rb->t->size, rb->k);

      addeval_amatrix_avector(alpha, h2->f, xp, yp);

      uninit_avector(yp);
      uninit_avector(xp);
    }
  }
  if((dc->active) && (rsons + csons != 0))
  {

          uint ytoff = rb->k;

    for(uint i = 0; i < rsons; ++i)
    {
      assert(rsons == 1 || rb->sons > 0);
      pavector yt1 = (rb->sons > 0                                          ?
                      init_sub_avector(&loc2, yt, rb->son[i]->ktree, ytoff) :
                      init_sub_avector(&loc2, yt, rb->ktree, 0));

      uint xtoff   = cb->k;

      for(uint j = 0; j < csons; ++j)
      {

        pavector xt1 = (cb->sons > 0                                          ?
                        init_sub_avector(&loc1, xt, cb->son[j]->ktree, xtoff) :
                        init_sub_avector(&loc1, xt, cb->ktree, 0));

        fastaddeval_distributed_h2matrix_avector(alpha,
                                                 h2->son[i + j * rsons],
                                                 xt1,
                                                 yt1);

        uninit_avector(xt1);

        xtoff += (cb->sons > 0 ? cb->son[j]->ktree : cb->t->size);
      }

      assert(xtoff == cb->ktree);

      uninit_avector(yt1);

      ytoff += (rb->sons > 0 ? rb->son[i]->ktree : rb->t->size);
    }

    assert(ytoff == rb->ktree);
  }
}

void
fastaddevaltrans_distributed_h2matrix_avector(field      alpha,
                                              pch2matrix h2,
                                              pavector   xt,
                                              pavector   yt)
{
  pcclusterbasis rb    = h2->rb;
  pcclusterbasis cb    = h2->cb;
  const uint     csons = h2->csons;
  const uint     rsons = h2->rsons;

  avector        loc1, loc2;
  int            world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  pcdstbcluster dc = (pcdstbcluster) rb->t;

  if(world_rank == dc->resp)
  {
    if (h2->u)
    {
      addevaltrans_amatrix_avector(alpha, &h2->u->S, xt, yt);
    }
    else if (h2->f)
    {
      pavector xp = init_sub_avector(&loc1, xt, rb->t->size, rb->k);
      pavector yp = init_sub_avector(&loc2, yt, cb->t->size, cb->k);

      addevaltrans_amatrix_avector(alpha, h2->f, xp, yp);

      uninit_avector(yp);
      uninit_avector(xp);
    }
  }
  if (dc->active && h2->son)
  {
    uint ytoff = cb->k;

    for(uint j = 0; j < csons; ++j)
    {
      pavector yt1 = (cb->sons > 0 ?
                      init_sub_avector(&loc2, yt, cb->son[j]->ktree, ytoff) :
                      init_sub_avector(&loc2, yt, cb->ktree, 0));

      uint xtoff   = rb->k;

      for(uint i = 0; i < rsons; i++)
      {

        pavector xt1 = (rb->sons > 0 ?
                        init_sub_avector(&loc1, xt, rb->son[i]->ktree, xtoff) :
                        init_sub_avector(&loc1, xt, rb->ktree, 0));

        fastaddevaltrans_distributed_h2matrix_avector(alpha,
                                                      h2->son[i + j * rsons],
                                                      xt1,
                                                      yt1);

  	    uninit_avector(xt1);

  	    xtoff += (rb->sons > 0 ? rb->son[i]->ktree : rb->t->size);
      }

      assert(xtoff == rb->ktree);

      uninit_avector(yt1);

      ytoff += (cb->sons > 0 ? cb->son[j]->ktree : cb->t->size);
    }

    assert(ytoff == cb->ktree);
  }
}

void
addeval_distributed_h2matrix_avector(field      alpha,
                                     pch2matrix h2,
                                     pcavector  x,
                                     pavector   y)
{
  pavector xt = new_coeffs_clusterbasis_avector(h2->cb);
  pavector yt = new_coeffs_clusterbasis_avector(h2->rb);

  clear_avector(yt);

  forward_distributed_clusterbasis_avector(h2->cb, x, xt, 1);

  MPI_Bcast(xt->v, xt->dim, MPI_REAL_H2, 0, MPI_COMM_WORLD);

  fastaddeval_distributed_h2matrix_avector(alpha, h2, xt, yt);

  backward_distributed_clusterbasis_avector(h2->rb, yt, y, 1);

  del_avector(yt);
  del_avector(xt);
}

void
addevaltrans_distributed_h2matrix_avector(field      alpha,
                                          pch2matrix h2,
                                          pcavector  x,
			                                    pavector   y)
{
  pavector xt = new_coeffs_clusterbasis_avector(h2->rb);
  pavector yt = new_coeffs_clusterbasis_avector(h2->cb);

  clear_avector(yt);

  forward_distributed_clusterbasis_avector(h2->rb, x, xt, 1);

  fastaddevaltrans_distributed_h2matrix_avector(alpha, h2, xt, yt);

  real* localsum = (real *) malloc(sizeof(real) * yt->dim);

  MPI_Allreduce(yt->v, localsum, yt->dim, MPI_REAL_H2, MPI_SUM, MPI_COMM_WORLD);

  free(yt->v);

  yt->v = localsum;

  backward_distributed_clusterbasis_avector(h2->cb, yt, y, 0);

  del_avector(yt);
  del_avector(xt);
}

real
norm2diff_amatrix_distributed_h2matrix(pch2matrix a, pcamatrix b)
{
  avector tmp1, tmp2;
  int     world_rank;

  const uint rows = a->rb->t->size;
  const uint cols = a->cb->t->size;

  assert(b->rows == rows);
  assert(b->cols == cols);

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  pavector x = init_avector(&tmp1, cols);
  pavector y = init_avector(&tmp2, rows);

  if(world_rank == 0)
    random_avector(x);

  MPI_Bcast(x->v, x->dim, MPI_REAL_H2, 0, MPI_COMM_WORLD);

  real norm = norm2_avector(x);
  for(uint i = 0; i < NORM_STEPS && norm > 0.0; ++i)
  {
    scale_avector(1.0 / norm, x);

    clear_avector(y);
    addeval_distributed_h2matrix_avector(1.0, a, x, y);
    addeval_amatrix_avector(-1.0, b, x, y);

    clear_avector(x);
    addevaltrans_distributed_h2matrix_avector(1.0, a, y, x);
    addevaltrans_amatrix_avector(-1.0, b, y, x);

    norm = norm2_avector(x);
  }

  uninit_avector(y);
  uninit_avector(x);

  return REAL_SQRT(norm);
}
