/* ------------------------------------------------------------
 * This is the file "basicMPI.h" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

/** @file   basicMPI.h
 *  @author Bennet Carstensen
 */

#ifndef BASIC_MPI_H
#define BASIC_MPI_H

/** @defgroup basicMPI basicMPI
 *  @brief Miscellaneous auxiliary functions and macros for MPI.
 *
 * @{
 */

#include <mpi.h>
#include "basic.h"

/** @brief real floating point type for MPI.
 *
 * This type is used, e.g., for geometric coordinates, norms and
 * diagonal elements of self-adjoint matrices in MPI.
*/
#ifdef USE_FLOAT
#define MPI_REAL_H2 MPI_FLOAT
#else
#define MPI_REAL_H2 MPI_DOUBLE
#endif

/**
 * @}
 */

#endif // BASIC_MPI_H
