/* ------------------------------------------------------------
 * This is the file "clusterbasisMPI.c" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

#include "clusterbasisMPI.h"

#include "basicMPI.h"

void
resize_distributed_clusterbasis(pclusterbasis cb, int k)
{
  int  world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  pcdstbcluster dc = (pcdstbcluster) cb->t;

  if(world_rank == dc->resp)
  {
    if (cb->sons > 0)
    {
      for(uint i = 0; i < cb->sons; i++) {
        resize_amatrix(&cb->son[i]->E, cb->son[i]->k, k);
      }
    }
    else
      resize_amatrix(&cb->V, cb->t->size, k);
  }

  cb->k = k;

  update_distributed_clusterbasis(cb);
}

void
update_distributed_clusterbasis(pclusterbasis cb)
{
  uint stree, sbranch;
  uint sons;
  int  world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  stree = sbranch = 0;
  sons  = cb->sons;

  if(sons == 0)
  {
    stree   += cb->t->size;
    sbranch += cb->t->size;
  }
  else
  {
    for(uint i = 0; i < sons; ++i)
    {
      pcdstbcluster dc = (pcdstbcluster) cb->son[i]->t;

      MPI_Bcast(&cb->son[i]->ktree, 1, MPI_UNSIGNED, dc->resp, MPI_COMM_WORLD);

      stree += cb->son[i]->ktree;

      // TODO: Update kbranch for distributed clusterbasis
    }
  }

  cb->ktree   = stree + cb->k;
  cb->kbranch = sbranch + cb->k;
}

void
forward_distributed_clusterbasis_avector(pcclusterbasis cb,
                                         pcavector      x,
				                                 pavector       xt,
                                         uint           pardepth)
{
  int  world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  assert(xt->dim == cb->ktree);

  pcdstbcluster dc = (pcdstbcluster) cb->t;

  if(dc->active)
  {
    const uint sons = cb->sons;

    avector    loc1;
    pavector   xc = init_sub_avector(&loc1, xt, cb->k, 0);

    clear_avector(xc);

    pavector*  xt1;

    if(sons > 0)
    {
      uint xtoff = cb->k;

      xt1 = (pavector *) allocmem((size_t) sizeof(pavector) * sons);

      for (uint i = 0; i < sons; i++)
      {
        xt1[i] = new_sub_avector(xt, cb->son[i]->ktree, xtoff);

        xtoff += cb->son[i]->ktree;
      }

      assert(xtoff == cb->ktree);

      const uint nthreads = sons;
      (void) nthreads;

      for (uint i = 0; i < sons; ++i) {

        forward_distributed_clusterbasis_avector(cb->son[i],
                                                 x,
                                                 xt1[i],
          					                             (pardepth > 0 ? pardepth - 1
                                                               : 0));

        pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

        /* The node responsibile for the current son sends his sub vector */
        if((world_rank != dc->resp) && (world_rank == dc1->resp))
          MPI_Send(xt1[i]->v,
                   cb->son[i]->ktree,
                   MPI_REAL_H2,
                   dc->resp,
                   0,
                   MPI_COMM_WORLD);
      }

      for (uint i = 0; i < sons; ++i)
      {
        pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

        /*
         * The node responsibile for the current cluster tries to receive the
         * previously sended sub_vector
         */
        if((world_rank == dc->resp) && (world_rank != dc1->resp))
          MPI_Recv(xt1[i]->v,
                   cb->son[i]->ktree,
                   MPI_REAL_H2,
                   dc1->resp,
                   0,
                   MPI_COMM_WORLD,
                   MPI_STATUS_IGNORE);
      }

      /*
       * The node responsible for the cluster calculates the mv-product of all
       * transfer matrices of his sons with their cooresponding sub vectors
       */
      if(world_rank == dc->resp)
        for(uint i = 0; i < cb->sons; ++i)
          mvm_amatrix_avector(1.0, true, &cb->son[i]->E, xt1[i], xc);

      for(uint i = 0; i < cb->sons; i++)
        del_avector(xt1[i]);

      freemem(xt1);
    }
    else
      /*
       * The node responsible calculates the mv-product of the leaf matrix and
       * the corresponding sub vector
       */
      if(world_rank == dc->resp)
      {
        avector  loc2;
        pavector xp = init_sub_avector(&loc2, xt, cb->t->size, cb->k);

        for(uint i = 0; i < cb->t->size; i++)
          setentry_avector(xp, i, getentry_avector(x, cb->t->idx[i]));

        mvm_amatrix_avector(1.0, true, &cb->V, xp, xc);

        uninit_avector(xp);
      }

    uninit_avector(xc);
  }
}

void
backward_distributed_clusterbasis_avector(pcclusterbasis cb,
                                          pavector       yt,
				                                  pavector       y,
                                          uint           pardepth)
{
  avector   loc1, loc2;
  pavector *yt1, *ya;

  int       world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  assert(yt->dim == cb->ktree);

  pavector yc = init_sub_avector(&loc1, yt, cb->k, 0);

  pcdstbcluster dc = (pcdstbcluster) cb->t;

  if(cb->sons == 0)
  {
    pavector yp = init_sub_avector(&loc2, yt, cb->t->size, cb->k);

    if(world_rank == dc->resp)
      mvm_amatrix_avector(1.0, false, &cb->V, yc, yp);

    MPI_Bcast(yp->v, cb->t->size, MPI_REAL_H2, dc->resp, MPI_COMM_WORLD);

    for(uint i = 0; i < cb->t->size; ++i)
      addentry_avector(y, cb->t->idx[i], getentry_avector(yp, i));

    uninit_avector(yp);
  }
  else
  {
    uint ytoff = cb->k;

    yt1 = (pavector *) allocmem((size_t) sizeof(pavector) * cb->sons);
    ya  = (pavector *) allocmem((size_t) sizeof(pavector) * cb->sons);

    for(uint i = 0; i < cb->sons; ++i)
    {
      yt1[i] = new_sub_avector(yt, cb->son[i]->ktree, ytoff);

      pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

      if(((world_rank == dc->resp) && (world_rank != dc1->resp)) ||
         ((world_rank != dc->resp) &&(world_rank == dc1->resp)))
        ya[i]  = new_zero_avector(cb->son[i]->k);

      ytoff += cb->son[i]->ktree;
    }

    assert(ytoff == cb->ktree);

    if(world_rank == dc->resp)
    {
      for(uint i = 0; i < cb->sons; ++i)
      {
        pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

        if(world_rank == dc1->resp)
          mvm_amatrix_avector(1.0, false, &cb->son[i]->E, yc, yt1[i]);
        else
        {
          mvm_amatrix_avector(1.0, false, &cb->son[i]->E, yc, ya[i]);
        }
      }
    }

    if(dc->active)
    {
      for(uint i = 0; i < cb->sons; ++i)
      {
        pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

        if((world_rank == dc->resp) && (world_rank != dc1->resp))
          MPI_Send(ya[i]->v, cb->son[i]->k, MPI_REAL_H2, dc1->resp, 0, MPI_COMM_WORLD);
      }

      for(uint i = 0; i < cb->sons; ++i)
      {
        pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

        if((world_rank != dc->resp) && (world_rank == dc1->resp))
        {
          MPI_Recv(ya[i]->v, cb->son[i]->k, MPI_REAL_H2, dc->resp, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

          for(uint j = 0; j < cb->son[i]->k; j++)
            addentry_avector(yt1[i], j, getentry_avector(ya[i], j));
        }
      }
    }

    for(uint i = 0; i < cb->sons; ++i)
    {
      backward_distributed_clusterbasis_avector(cb->son[i], yt1[i], y,
        (pardepth > 0 ? pardepth - 1 : 0));

      pcdstbcluster dc1 = (pcdstbcluster) cb->son[i]->t;

      if(((world_rank == dc->resp) && (world_rank != dc1->resp)) ||
         ((world_rank != dc->resp) && (world_rank == dc1->resp)))
        del_avector(ya[i]);
    }

    freemem(ya);

    for(uint i = 0; i < cb->sons; ++i)
      del_avector(yt1[i]);

    freemem(yt1);
  }

  uninit_avector(yc);
}
