/* ------------------------------------------------------------
 * This is the file "clusterbasisMPI.h" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

 /**
  * @file clusterbasisMPI.h
  * @author Bennet Carstensen
  */

#ifndef CLUSTERBASIS_MPI_H
#define CLUSTERBASIS_MPI_H

/** @defgroup clusterbasisMPI clusterbasisMPI
 * @brief Extension of cluster bases for H2-matrices for MPI.
 *
 * Extends the clusterbasis class with functions to operate on
 * distributed systems.
 * @{ */

#include "clusterbasis.h"

#include "clusterMPI.h"

/** @brief Change the rank of a clusterbasis and resize
 *  <tt>cb->V,</tt> <tt>cb->son[i]->E</tt> for all sons for which the node is responsible.
 *
 *  @param cb Clusterbasis that will be changed.
 *  @param k New rank, i.e., number of columns of <tt>V</tt> or <tt>cb->son[i]->E</tt>. */
HEADER_PREFIX void
resize_distributed_clusterbasis(pclusterbasis cb, int k);


/** @brief Updates bookkeeping information, e.g., <tt>cb->ktree,</tt> for
 *  a clusterbasis object after its sons have been altered. The node which is responsible sends the informations to all other nodes.
 *
 *  @param cb clusterbasis that will be updated. */
HEADER_PREFIX void
update_distributed_clusterbasis(pclusterbasis cb);

/** @brief Forward transformation of a distributed cluster basis.
 *
 * If <tt>cb</tt> is active and not a leaf, check the responsibility for
 * <tt>cb</tt>.
 * If the node is not responsibile for <tt>cb</tt> but for the son, then send
 * the sub_vector, which is computed for the son, to the responsible node.
 * If the node is responsibile for <tt>cb</tt> but not for the son, then
 * receive the values.
 *
 * Is the node responsible for <tt>cb</tt> and has sons, then compute the
 * transfer matrices \f$E_t\f$.
 * If it is a leaf, compute  \f$\hat x_t = V_t^* x\f$.
 *
 *  @param cb       Distributed cluster basis.
 *  @param x        Source vector.
 *  @param xt       Target vector of dimension <tt>cb->ktree</tt>, will
 *                  be filled with a mix of transformed coefficients and
 *                  permuted coefficients.
 *  @param pardepth Depth of the parallelization on the node.
 */
HEADER_PREFIX void
forward_distributed_clusterbasis_avector(pcclusterbasis cb,
                                         pcavector      x,
				                                 pavector       xt,
                                         uint           pardepth);


/** @brief Backward transformation of a distributed cluster basis.
 *
 * If <tt>cb</tt> is a leaf, compute \f$y \gets y + V_t \widehat y_t\f$. If it
 * is not a leaf and it is responsible for <tt>cb</tt>, compute the transfer
 * matrices.
 *
 * Is <tt>cb</tt> active, then check for every son, if the node is responsible
 * for <tt>cb</tt> but not for the son, send the subvector to the son. And then
 * receive this for all sons, if the node is not responsible for <tt>cb</tt>
 * but for the son and add it to \f$\widehat y_t\f$.
 *
 *  @param cb Cluster basis.
 *  @param yt Source vector of dimension <tt>cb->ktree</tt>, filled
 *         with a mix of transformed coefficients and permuted coefficients.
 *         This vector will be overwritten by the function.
 *  @param y Target vector.
 *  @param pardepth Depth of the parallelization on the node.
 */
HEADER_PREFIX void
backward_distributed_clusterbasis_avector(pcclusterbasis cb,
                                          pavector       yt,
				                                  pavector       y,
                                          uint           pardepth);

#endif // CLUSTERBASIS_MPI_H
