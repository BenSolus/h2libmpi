## H2LibMPI

An extension for the [H2Lib](http://h2lib.org), enabling distribution of H2-matrices and performing distributed H2-Matrix-Vector multiplication across all nodes of a distributed system through [Open MPI](https://www.open-mpi.org/).

### Prerequisite

* [H2Lib](https://github.com/H2Lib/H2Lib/tree/master) (Tested with Version 2.0 and 3.0)
* Open MPI (other implementations might work, but untested
* An working implementation of the [GCC](https://gcc.gnu.org/) and [CMake](https://cmake.org/)

### Installation

Just execute the install script with the include and link directory as arguments

```sh install.sh -i /path/to/include/dir -l /path/to/link/dir```

The headers of H2LibMPI will bei located at ```/path/to/include/dir/mpi```, the library at ```/path/to/link/dir/libh2mpi.a``` and the examples in ```/path/to/link/dir/Examples/mpi```.
