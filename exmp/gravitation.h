/* ------------------------------------------------------------
 * This is the file "gravitation.h" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

/** @file gravitation.h
  * @author Bennet Carstensen
  */

#ifndef GRAVITATION_H
#define GRAVITATION_H

#include "clusterMPI.h"

#include "clusterbasis.h"
#include "h2matrix.h"

/** @defgroup gravitation gravitation
 *  @brief Representation of objects with masses in three-dimensional space.
 *
 * The @ref grav class represents objects with a location in
 * three-dimensional space and a corresponding mass. Basing on this object the
 * @ref clustergeometry object and the H2-matrices are build for calculating
 * the force of each object.
 *
 * @{ */

 /** @brief Representation of objects with masses in three-dimensional space. */
typedef struct _grav grav;

/** @brief Pointer to @ref grav object. */
typedef grav* pgrav;

/** @brief Pointer to constant @ref grav object.*/
typedef const grav* pcgrav;

 /** @brief Representation of objects with masses in three-dimensional space.
  *
  * One @ref grav object represents objects with masses which in turn are
  * represented by three coordinates in the three-dimensional space and a
  * corresponding mass.
  */
struct _grav{
  /** @brief Locations of masses */
  real **x;

  /** @brief Masses */
  real *m;

  /** @brief Number of masses */
  uint n;

  /** @brief Forces */
  real **f;

  /** @brief Includes the locations and masses */
  real *mem;

  /** @brief Order of interpolation*/
  uint im;

  /** @brief Interpolation points in [-1,1] */
  real *ix;
};

 /** @brief Create a new @ref grav object.
 *
 *  Allocates storage for the object and sets up its components.
 *
 *  @remark Should always be matched by a call to @ref del_grav.
 *
 *  @param n Number of masses
 *  @param im Order of interpolation
 *  @return Returns the newly created @ref grav object.
 */
HEADER_PREFIX pgrav
new_grav(uint n, uint im);

/** @brief Delete a @ref grav object.
 *
 *  Releases the storage corresponding to the object.
 *
 *  Only objects with <tt>cb->refs==0</tt> may be deleted.
 *
 *  @param gv Object to be deleted. */
HEADER_PREFIX void
del_grav(grav *gv);

/** @brief Fill the coordinates of a grav object with random values on one node and distributes them to all nodes.
 *
 *  @param a Target grav objects. */
HEADER_PREFIX void
random_distributed_grav(pgrav gv);

/** @brief Construct a @ref clustergeometry from a @ref grav object.
 *
 *  All locations will be set.
 *
 *  @param gv Grav object.
 *  @return Returns the built @ref clustergeometry object. */
HEADER_PREFIX pclustergeometry
build_from_grav_clustergeometry(pgrav gv);

/** @brief Creates the nearfieldmatrix.
 *
 *  The nearfieldmatrix will be set with modified values of the grav object.
 *
 *  @param ridx Row indices.
 *  @param cidx Column indices.
 *  @param gv   Grav object.
 *  @param N    Nearfieldmatrix.
 *  */
HEADER_PREFIX void
nearfield_grav(uint *ridx, uint *cidx, pgrav gv, pamatrix N);

/** @brief Preparing the matrices \f$V_t\f$ and the transfer matrices including the responsibility condition.
 *
 * If the node is responsible for a leaf cluster t, then prepare \f$V_t\f$
 * else prepare the transfer matrices \f$E_t'\f$ for all sons of <tt>t</tt> if .
 * the node is responsible for t.
 *
 *  @param t Cluster to create clusterbasis.
 *  @param gv Grav object.
 *  @return Returns a @ref clusterbasis object.*/
HEADER_PREFIX pclusterbasis
build_from_grav_clusterbasis(pcdstbcluster t, grav *gv);

/** @brief Fills the H2-matrix.
 *
 * Is the condition <tt>G->f</tt> satisfied so execute @ref nearfield_grav. If
 * <tt>G->u</tt> is satisfied, so it is an admissible block and we can compute
 * \f$S\f$. Otherwise we have to call @ref fill_h2matrix_grav recursively.
 *
 *  @param G     H2-matrix which will be filled.
 *  @param gv    Grav object.
 *  @param depth Depth of the iteration.
 * */
HEADER_PREFIX void
fill_from_grav_h2matrix(ph2matrix G, pgrav gv);

#endif // GRAVITATION_H
