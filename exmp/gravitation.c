/* ------------------------------------------------------------
 * This is the file "gravitation.c" of the H2LibMPI package.
 * All rights reserved, Bennet Carstensen 2016
 * ------------------------------------------------------------ */

#include "gravitation.h"

#include "basicMPI.h"
#include "clusterbasisMPI.h"

pgrav
new_grav(uint n, uint im)
{
  grav *gv;
  real *mem;
  uint i;

  gv = (grav *) allocmem(sizeof(grav));
  gv->x = (real **) allocmem(sizeof(real *) * n);
  gv->m = (real *) allocmem(sizeof(real) * n);
  gv->f = (real **) allocmem(sizeof(real *) * n);
  gv->n = n;

  gv->mem = mem = (real *) allocmem(sizeof(real) * 6 * n);

  for(i=0; i<n; i++) {
    gv->x[i] = mem;
    gv->f[i] = mem+3;
    mem += 6;
  }

  gv->im = im;
  gv->ix = (real *) allocmem(sizeof(real) * im);

  for(i=0; i<im; i++)
    gv->ix[i] = REAL_COS(M_PI * (i+0.5) / im);

  return gv;
}

void
del_grav(pgrav gv)
{
  if(gv->ix != NULL) {
    freemem(gv->ix);
    gv->ix = NULL;
  }
  if(gv->mem != NULL) {
    freemem(gv->mem);
    gv->mem = NULL;
  }
  if(gv->f != NULL) {
    freemem(gv->f);
    gv->f = NULL;
  }
  if(gv->m != NULL) {
    freemem(gv->m);
    gv->m = NULL;
  }
  if(gv->x != NULL) {
    freemem(gv->x);
    gv->x = NULL;
  }
  if(gv != NULL) {
    freemem(gv);
    gv = NULL;
  }
}

/* ------------------------------------------------------------
 * Functions which are called intern
 * ------------------------------------------------------------ */

/** @brief Computing the interpolation points.
 *
 *  @param gv   Grav object.
 *  @param a    Minimal coordinate of the bounding box.
 *  @param b    Maximal coordinate of the bounding box.
 *  @return Returns a pointer to the computet values. */
static preal
build_interpolation_points(grav *gv, real a, real b)
{
  preal ix_ab = (preal) allocmem(sizeof(real) * gv->im);

  for(uint i = 0; i < gv->im; ++i)
    ix_ab[i] = 0.5 * (b+a) + 0.5 * (b-a) * gv->ix[i];

  return ix_ab;
}

/** @brief Evaluation of the lagrange value.
 *
 *  @param im   Order of interpolation.
 *  @param ix   Computet interpolation points.
 *  @param nu   Index of the iteration.
 *  @param y    Location.
 *  @return Returns the computet value. */
static real
eval_lagrange(uint im, real *ix, uint nu, real y)
{
  real vn, vd;

  vn = vd = 1.0;

  for(uint i = 0; i < nu; ++i)
  {
    vn *= (y - ix[i]);
    vd *= (ix[nu] - ix[i]);
  }

  for(uint i = nu+1; i < im; ++i)
  {
    vn *= (y - ix[i]);
    vd *= (ix[nu] - ix[i]);
  }

  return vn / vd;
}

void
random_distributed_grav(pgrav gv)
{
  int world_rank;

  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  for(uint i = 0; i < gv->n; ++i)
  {
    if(world_rank == 0)
    {
      gv->x[i][0] = REAL_RAND();
      gv->x[i][1] = REAL_RAND();
      gv->x[i][2] = REAL_RAND();
    }

    MPI_Bcast(gv->x[i], 3, MPI_REAL_H2, 0, MPI_COMM_WORLD);
  }
}

pclustergeometry
build_from_grav_clustergeometry(pgrav gv)
{
  pclustergeometry cg = new_clustergeometry(3, gv->n);

  for(uint i = 0; i < gv->n; ++i)
  {
    cg->x[i][0] = cg->smin[i][0] = cg->smax[i][0] = gv->x[i][0];
    cg->x[i][1] = cg->smin[i][1] = cg->smax[i][1] = gv->x[i][1];
    cg->x[i][2] = cg->smin[i][2] = cg->smax[i][2] = gv->x[i][2];
  }

  return cg;
}

void
nearfield_grav(uint *ridx, uint *cidx, pgrav gv, pamatrix G)
{
  #pragma omp for collapse(2)
  for(uint j = 0; j < G->cols; ++j)
    for(uint i = 0; i < G->rows; ++i)
    {
    uint  ii, jj;
    preal y;

    jj = (cidx ? cidx[j] : j);
    y  = gv->x[jj];
    ii = (ridx ? ridx[i] : i);

    if(ii == jj)
      setentry_amatrix(G, i, j, 0.0);
    else
    {
      real  norm, norm2, norm3;
      preal x;

      x     = gv->x[ii];

      norm2 = (REAL_SQR(x[0] - y[0]) +
               REAL_SQR(x[1] - y[1]) +
               REAL_SQR(x[2] - y[2]));
      norm  = REAL_SQRT(norm2);
      norm3 = norm * norm2;

      setentry_amatrix(G, i, j, (x[0] - y[0]) / norm3);
    }
  }
}

pclusterbasis
build_from_grav_clusterbasis(pcdstbcluster dc, pgrav gv)
{
  const uint    im = gv->im;
  
  pclusterbasis cb;

  cb = new_clusterbasis((pccluster) dc);

  if(dc->c.sons == 0)
  {
    resize_clusterbasis(cb, im*im*im);

    preal ix0 = build_interpolation_points(gv, dc->c.bmin[0], dc->c.bmax[0]);
    preal ix1 = build_interpolation_points(gv, dc->c.bmin[1], dc->c.bmax[1]);
    preal ix2 = build_interpolation_points(gv, dc->c.bmin[2], dc->c.bmax[2]);

    pamatrix V = &cb->V;

  	for(uint i = 0; i < dc->c.size; ++i)
    {
  		preal x = gv->x[dc->c.idx[i]];

      for(uint nu0 = 0; nu0 < im; ++nu0)
      {
  			real val0 = eval_lagrange(im, ix0, nu0, x[0]);

        for(uint nu1 = 0; nu1 < im; ++nu1)
        {
  				real val1 = val0 * eval_lagrange(im, ix1, nu1, x[1]);

          for(uint nu2 = 0; nu2 < im; ++nu2)
          {
  	  			real val2 = val1 * eval_lagrange(im, ix2, nu2, x[2]);

  	  			setentry_amatrix(V, i, nu0+im*(nu1+im*nu2), val2);
  				}
  			}
  		}
  	}

    freemem(ix2);
    freemem(ix1);
    freemem(ix0);
  }
  else
  {
    for(uint i = 0; i < cb->sons; ++i)
    {
    	pclusterbasis cb1 = build_from_grav_clusterbasis
                          (
                            (pcdstbcluster) dc->c.son[i],
                            gv
                          );

    	ref_clusterbasis(cb->son+i, cb1);
    }

    resize_clusterbasis(cb, im*im*im);

    preal ix0 = build_interpolation_points(gv, dc->c.bmin[0], dc->c.bmax[0]);
    preal ix1 = build_interpolation_points(gv, dc->c.bmin[1], dc->c.bmax[1]);
    preal ix2 = build_interpolation_points(gv, dc->c.bmin[2], dc->c.bmax[2]);

	 for(uint i = 0; i < cb->sons; ++i)
   {
	    preal	iy0 = build_interpolation_points(gv,
				                                     dc->c.son[i]->bmin[0],
                                             dc->c.son[i]->bmax[0]);
		  preal iy1 = build_interpolation_points(gv,
				                                     dc->c.son[i]->bmin[1],
                                             dc->c.son[i]->bmax[1]);
		  preal iy2 = build_interpolation_points(gv,
				                                     dc->c.son[i]->bmin[2],
                                             dc->c.son[i]->bmax[2]);

    pamatrix E = &cb->son[i]->E;

		for(uint mu0 = 0; mu0 < im; ++mu0)
			for(uint nu0 = 0; nu0 < im; ++nu0)
      {
				real val0 = eval_lagrange(im, ix0, nu0, iy0[mu0]);

        for(uint mu1 = 0; mu1 < im; ++mu1)
		 			for(uint nu1 = 0; nu1 < im; ++nu1)
          {
						real val1 = val0 * eval_lagrange(im, ix1, nu1, iy1[mu1]);

            for(uint mu2 = 0; mu2 < im; ++mu2)
							for(uint nu2 = 0; nu2 < im; ++nu2)
              {
								real val2 = val1 * eval_lagrange(im, ix2, nu2, iy2[mu2]);

                setentry_amatrix(E,
													       mu0+im*(mu1+im*mu2), nu0+im*(nu1+im*nu2),
						 							       val2);
							}
					}
	    	}

    freemem(iy2);
    freemem(iy1);
    freemem(iy0);
	}

  freemem(ix2);
  freemem(ix1);
  freemem(ix0);
}

  return cb;
}

void
fill_from_grav_h2matrix(ph2matrix G, pgrav gv)
{
  pccluster rc = G->rb->t;
  pccluster cc = G->cb->t;

  if(G->f)
    nearfield_grav(rc->idx, cc->idx, gv, G->f);
  else if(G->u)
  {
    pamatrix S   = &G->u->S;

    preal    ix0 = build_interpolation_points(gv, rc->bmin[0], rc->bmax[0]);
    preal    ix1 = build_interpolation_points(gv, rc->bmin[1], rc->bmax[1]);
    preal    ix2 = build_interpolation_points(gv, rc->bmin[2], rc->bmax[2]);

    preal    iy0 = build_interpolation_points(gv, cc->bmin[0], cc->bmax[0]);
    preal    iy1 = build_interpolation_points(gv, cc->bmin[1], cc->bmax[1]);
    preal    iy2 = build_interpolation_points(gv, cc->bmin[2], cc->bmax[2]);

    uint im = gv->im;

    #pragma omp for collapse(4)
    for(uint mu0 = 0; mu0 < im; ++mu0)
      for(uint nu0 = 0; nu0 < im; ++nu0)
    		for(uint mu1 = 0; mu1 < im; ++mu1)
    			for(uint nu1 = 0; nu1 < im; ++nu1)
    				for(uint mu2 = 0; mu2 < im; ++mu2)
    					for(uint nu2 = 0; nu2 < im; ++nu2)
              {
                real  norm, norm2, norm3;
                field val;

    						norm2 = (REAL_SQR(ix0[nu0] - iy0[mu0]) +
      									 REAL_SQR(ix1[nu1] - iy1[mu1]) +
      									 REAL_SQR(ix2[nu2] - iy2[mu2]));
    						norm  = REAL_SQRT(norm2);
    						norm3 = norm2 * norm;

    						val   = (ix0[nu0] - iy0[mu0]) / norm3;

    						setentry_amatrix(S,
                                 nu0+im*(nu1+im*nu2),
                                 mu0+im*(mu1+im*mu2),
      											     val);
      				}

    freemem(iy2);
    freemem(iy1);
    freemem(iy0);
    freemem(ix2);
    freemem(ix1);
    freemem(ix0);
  }

  if(G->son)
  {
    uint rsons = G->rsons;
    uint csons = G->csons;

    for(uint j=0; j < csons; ++j)
      for(uint i=0; i < rsons; ++i)
        fill_from_grav_h2matrix(G->son[i+rsons*j], gv);
  }
}
